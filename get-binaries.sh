#!/bin/sh

arch=""

if [ "$(uname -m)" = "x86_64" ]; then
  arch="amd64"
elif [ "$(uname -m)" = "aarch64" ]; then
  arch="arm64"
fi

wget "https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-${arch}-v1.1.1.tgz" -O - | tar xvz
