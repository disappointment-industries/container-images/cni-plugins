FROM docker.io/alpine:latest as build

ADD get-binaries.sh /usr/local/bin/
WORKDIR /opt/cni/bin
RUN get-binaries.sh

FROM docker.io/alpine:latest

WORKDIR /opt/cni/bin

COPY --from=build /opt/cni/bin/. /opt/cni/bin/.

CMD [ "echo", "This image is solely for copying CNI plugins to the host, it will no do anything by itself." ]
